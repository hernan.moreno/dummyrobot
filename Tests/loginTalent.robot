*** Settings ***
Documentation  Login Functionality
Library  SeleniumLibrary


*** Variables ***

*** Test Cases ***
Verify Successful Login
    [documentation]  This test case verifies that user is able to successfully Login to OrangeHRM
    [tags]  Smoke
    Open Browser  https://stage-beauty.allworknow.com/login.html  Chrome
    Maximize Browser Window
    Wait Until Element Is Visible  css:input[name='username']  timeout=5
    Input Text  css:input[name='username']  salesrep
    Input Password  css:input[name='password']  socket123
    Click Element  css:input[type='submit']
    Wait Until Element Is Visible  xpath = //*[contains(text(), 'John Goodman')]  timeout=19
    Element Should Be Visible  xpath = //*[contains(text(), 'John Goodman')]  timeout=19
    #Click Element  xpath = //span[contains(text(), 'Calendar')]
    #Click Element  xpath = //span[contains(text(), 'View Calendar')]
    #Wait Until Element Is enabled  xpath = //button[contains(text(), 'CREATE BOOKING +')]  timeout=9
    #Wait until element is not visible  css:div[class="react-loader"]  timeout=9
    #Click Element  xpath = //button[contains(text(), 'CREATE BOOKING +')]
    #Element Should Be Visible  xpath = //h1[contains(text(), 'Booking Details')]  timeout=19
    #Sleep  10s
    Wait Until Element Is Visible  //a[contains(.,'booking requests')][@href='/booking/pending']  timeout=19
    Click Element  xpath = //a[contains(.,'booking requests')][@href='/booking/pending']
    #Click Element  xpath = //div[1]/div/div[2]/button/span[contains(.,'Onboard')]
    #Element Should Be Visible    xpath = //div[@id='onboard_dialog']//h5[contains(.,'New Onboarding Request')]
    #Input Text  css:input[name='firstName']  firstName
    #Input Text  css:input[name='lastName']  lastName
    #Input Text  css:input[name='email']  email
    #Input Text  css:input[name='rate']  rate
    #Input Text  css:input[name='address']  address
    #Input Text  css:input[name='city']  city
    #Input Text  css:input[name='zip']  zip
    #Input Text  css:input[name='phone']  phone

    #Wait until element is visible  xpath = //label[contains(text(), 'Country')]  timeout=9
    #Click Element  xpath = //body/div[2]/div[3]/div/form/div[2]/div/div[2]/div/div[4]/div/div/div/div/button[2]/span[1]
    #Click Element  xpath = //li[contains(text(), 'US')]
    #Click Element  //body/div[2]/div[3]/div/form/div[2]/div/div[2]/div/div[5]/div/div/div/div/button[2]/span[1]
    #Wait until element is visible  xpath = //li[contains(text(), 'AK')]  timeout=9
    #Click Element  xpath = //li[contains(text(), 'AK')]
    #Sleep  5s
    #Click Element  xpath = //*[contains(text(), 'Cancel')]
    Sleep  15s
    #Click Element  xpath =//button[@type='submit']/span[contains(text(), 'Save')]
    #Close Browser

    #//*[@id="reactappdiv"]/div/main/div[2]/div[3]/div[@class='MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-sm-6 MuiGrid-grid-md-4']

#//span[contains(.,'ACCEPT')]
#//span[contains(.,'DECLINE')]