*** Settings ***
Documentation  Login Functionality
Library  SeleniumLibrary
Test Setup  Open Firefox

*** Variables ***
*** Keywords ***
Open Firefox
    ${options}=    Evaluate    sys.modules['selenium.webdriver'].FirefoxOptions()
    Call Method    ${options}    add_argument    --disable-extensions
    Call Method    ${options}    add_argument    --headless
    Call Method    ${options}    add_argument    --disable-gpu
    Call Method    ${options}    add_argument    --no-sandbox
    Create Webdriver    Firefox    options=${options}

*** Test Cases ***
Verify Successful Login
    [documentation]  This test case verifies that user is able to successfully Login to OrangeHRM
    [tags]  Smoke
    Go To  https://stage-beauty.allworknow.com/login.html  
    Maximize Browser Window
    Wait Until Element Is Visible  css:input[name='username']  timeout=5
    Input Text  css:input[name='username']  regionalmanager
    Input Password  css:input[name='password']  socket123
    Click Element  css:input[type='submit']
    Wait Until Element Is Visible  xpath = //*[contains(text(), 'Bill Murrays')]  timeout=19
    Element Should Be Visible  xpath = //*[contains(text(), 'Bill Murrays')]  timeout=19
    Capture page screenshot
    Sleep  5s

    #Click Element  xpath = //span[contains(text(), 'Talent')]
    #Click Element  xpath = //div[1]/div/div[2]/button/span[contains(.,'Onboard')]
    #Element Should Be Visible    xpath = //div[@id='onboard_dialog']//h5[contains(.,'New Onboarding Request')]
    #Input Text  css:input[name='firstName']  firstName
    #Input Text  css:input[name='lastName']  lastName
    #Input Text  css:input[name='email']  email
    #Input Text  css:input[name='rate']  rate
    #Input Text  css:input[name='address']  address
    #Input Text  css:input[name='city']  city
    #Input Text  css:input[name='zip']  zip
    #Input Text  css:input[name='phone']  phone

    #Wait until element is visible  xpath = //label[contains(text(), 'Country')]  timeout=9
    #Click Element  xpath = //body/div[2]/div[3]/div/form/div[2]/div/div[2]/div/div[4]/div/div/div/div/button[2]/span[1]
    #Click Element  xpath = //li[contains(text(), 'US')]
    #Click Element  //body/div[2]/div[3]/div/form/div[2]/div/div[2]/div/div[5]/div/div/div/div/button[2]/span[1]
    #Wait until element is visible  xpath = //li[contains(text(), 'AK')]  timeout=9
    #Click Element  xpath = //li[contains(text(), 'AK')]
    #Sleep  5s
    #Click Element  xpath = //*[contains(text(), 'Cancel')]
    #Sleep  5s
    #Click Element  xpath =//button[@type='submit']/span[contains(text(), 'Save')]
    #Close Browser



    Click Element  xpath = //span[contains(text(), 'Calendar')]
    Sleep  5s
    Click Element  xpath = //span[contains(text(), 'View Calendar')]
    Sleep  5s
    Capture page screenshot
    Wait Until Element Is enabled  xpath = //button[contains(text(), 'CREATE BOOKING +')]  timeout=9
    Wait until element is not visible  css:div[class="react-loader"]  timeout=9
    Click Element  xpath = //button[contains(text(), 'CREATE BOOKING +')]
    Element Should Be Visible  xpath = //h1[contains(text(), 'Booking Details')]  timeout=19
    Wait until element is visible  xpath = //div[contains(@class,'error-message')]/label[contains(.,'BRAND')]/..//span[@class='Select-arrow-zone']/span[@class='Select-arrow']  timeout=9
    Click Element  xpath = //div[contains(@class,'error-message')]/label[contains(.,'BRAND')]/..//span[@class='Select-arrow-zone']/span[@class='Select-arrow']
    Sleep  5s
    Click Element  xpath = //*[contains(text(), 'Acme')]
    Sleep  5s
    Click Element  xpath = //div[contains(@class,'error-message')]/label[contains(.,'LOCATION')]/..//span[@class='Select-arrow-zone']/span[@class='Select-arrow']
    Sleep  5s
    Click Element  xpath = //*[contains(text(), 'Acme Retailer #2/ Acme Ca Location / Los Angeles, CA')]
    Sleep  5s
    Click Element  xpath = //div[contains(@class,'error-message')]/label[contains(.,'SELECT TALENT')]/..//span[@class='Select-arrow-zone']/span[@class='Select-arrow']
    Sleep  5s
    Capture page screenshot
    Sleep  5s